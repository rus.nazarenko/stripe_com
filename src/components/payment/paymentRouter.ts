import express from 'express'
const paymentRouter = express.Router()
import { paymentController } from './paymentController'


paymentRouter.post('/', paymentController)


export default paymentRouter
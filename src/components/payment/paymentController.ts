import {
  searchCustomerDB, fixResultsPaymentDB, createCustomerDB, createTransactionDB
} from './paymentService'
import { Request, Response } from 'express'
import { createPaymentPS, createCustomerPS } from '../../util/stripRequests'


export const paymentController = async (req: Request, res: Response) => {
  try {
    if (Object.entries(req.body).length === 0) { throw new Error("No data") }

    const paymentData = {
      user_id: req.body.user_id,
      order_id: req.body.order_id,
      amount: req.body.amount,
      token: req.body.token,
      email: 'ruslan.nazarenko@example.com'
    }

    const customerExists = await searchCustomerDB(paymentData.email)
    if (customerExists) {
      const currentTransaction = await createTransactionDB(paymentData, customerExists.dataValues.id)
      await createPaymentPS(paymentData, customerExists.id)
      await fixResultsPaymentDB(currentTransaction)

      // ========= Regular users can pay 2 more times :) =========
      const currentTransaction2 = await createTransactionDB(paymentData, customerExists.dataValues.id)
      await createPaymentPS(paymentData, customerExists.id)
      await fixResultsPaymentDB(currentTransaction2)
      const currentTransaction3 = await createTransactionDB(paymentData, customerExists.dataValues.id)
      await createPaymentPS(paymentData, customerExists.id)
      await fixResultsPaymentDB(currentTransaction3)
      // ========================================================= /
    } else {
      const newCustomer = await createCustomerPS(paymentData)
      await createCustomerDB(newCustomer)
      const currentTransaction = await createTransactionDB(paymentData, newCustomer.id)
      await createPaymentPS(paymentData, newCustomer.id)
      await fixResultsPaymentDB(currentTransaction)
    }
    res.status(201).json({ success: true })
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}
const db = require("../../db/models")
import { currency } from '../../config/stripe'


export const createTransactionDB = async (payload, customer_id) => {
  try {
    const currenCustomer = await db.customer.findOne({ where: { id: customer_id } })
    if (!currenCustomer) throw new Error("No customer")
    payload.currency = currency
    const currentTransaction = await db.transaction.create(payload)
    await currenCustomer.setTransactions(currentTransaction)
    return currentTransaction
  } catch (err) {
    throw new Error(err)
  }
}

export const searchCustomerDB = async (email) => {
  try {
    const currenCustomer = await db.customer.findOne({ where: { email } })
    return currenCustomer
  } catch (err) {
    throw new Error(err)
  }
}

export const createCustomerDB = async (payload) => {
  try {
    await db.customer.create({
      email: payload.email,
      id: payload.id,
      phone: payload.phone
    })
  } catch (err) {
    throw new Error(err)
  }
}

export const fixResultsPaymentDB = async (currentTransaction) => {
  try {
    const result = await db.transaction.update({ successful: true }, {
      where: { id: currentTransaction.dataValues.id }
    })
  } catch (err) {
    throw new Error(err)
  }
}
'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class transaction extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.customer)
    }
  };
  transaction.init({
    order_id: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    amount: DataTypes.INTEGER,
    currency: DataTypes.STRING,
    successful: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'transaction',
    underscored: true
  });
  return transaction;
};
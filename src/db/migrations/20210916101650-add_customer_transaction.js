'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('transactions', 'customer_id', {
      type: Sequelize.STRING,
      references: {
        model: 'customers',
        key: 'id'
      },
      allowNull: true,
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE'
    })

  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('transactions', 'customer_id')
  }
}
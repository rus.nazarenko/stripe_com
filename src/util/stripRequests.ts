const { secret } = require('../config/stripe')
const stripe = require('stripe')(secret)
import { currency } from '../config/stripe'

export const createPaymentPS = async (paymentData, customerId) => {
  try {
    const charge = await stripe.charges.create({
      amount: paymentData.amount,
      currency: currency,
      description: `user_id: ${paymentData.user_id} paid ${paymentData.amount} for order - ${paymentData.order_id}`,
      customer: customerId
    })
  } catch (err) {
    console.log("stripeResData err ===========>>>>", err.type)
    throw new Error(err.type)
  }
}


export const createCustomerPS = async (payload) => {
  try {
    const customer = await stripe.customers.create({
      source: payload.token,
      email: payload.email
    })
    return customer
  } catch (err) {
    console.log("stripeResData err ===========>>>>", err.type)
    throw new Error(err.type)
  }
}







// const stripeRequest = async ({ order_id, user_id, token, amount }) => {
//   try {
//     const stripeResData = await stripe.charges.create({
//       amount,
//       currency: 'usd',
//       description: `user_id: ${user_id} paid ${amount} for order - ${order_id}`,
//       source: token,
//     })
//     // console.log("stripeResData ===>>", stripeResData)
//   } catch (err) {
//     console.log("stripeResData err ===>>", err.type)
//     throw new Error(err.type)
//   }
// }
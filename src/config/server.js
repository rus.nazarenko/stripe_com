module.exports = {
  port: process.env.APP_PORT || 3001,
  secret: process.env.JWT_SECRET
}
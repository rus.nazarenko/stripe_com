### Application launch 
docker-compose up --build

### Address for inquiries queries
POST http://localhost:3001/api/payment

### Secret key for stripe.com
The secret key must be specified in the .env file, in the SK_SECRET variable.